<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Action_model extends CI_Model {

    //beta version - no energy check
    function play($id_user, $id_card){
        
        $data = $this->getCardInfo($id_card);
        //$this->consoleMsg(""+var_dump($data));
        
        
        
        if(!empty($data) && $data["location"] == 'hand'){
            return $this->analysePlay($data);
        }
        
        
        else if(!empty($data) || data["location"] != 'hand'){
            return "ilegal_move";
        }
            
        else if(empty($data["location"]))
        return "error";
        
        else
        return "unknown";    
         
        
        
    }
    
    function analysePlay($data){
        
        if(!empty($data)){
            
            $allow = true;
            $ext = $this->getCardProperties($data["id_card"]);
            $response;
            $dir = '';



            /* type analysis*/
            switch($ext["type"]){
                case null:                  echo 'fatal';
                                            break;
                
                case 0:                     $dir = 'fl';
                                            break;
                                       
                case 1:                     $dir = 'tr';
                                            break;
                
                case 2:                     $dir = 'hl';
                                            break;
                                        
                case 3:                     $dir = 'dp';
                                            break;
                                        
                case 4:                     $dir = 'target';
                                            break;
                                                        
                case 5:                     $dir = 'am';
                                            break;
                                       
                                        
                default:                    echo '';  
                                            break;
                
            }
            
            /* energy comparison */
            
            /* process */
            if($allow == true){
                //moving card
                 if($this->move($data["id_board"],$ext["type"]) == true){
                     

                     $response["result"] = "play";
                     $response["id_board"] = $data["id_card"];
                     $response["card_type"] = $ext["type"];
                     $response["card_direction"] =  $dir;
                     $response["card_num"] = $ext["card_num"];
                     $response["card_atk"] = $ext["atk"];
                     $response["card_def"] = $ext["def"];
                     $response["card_elem"] = "ne";

                     $this->consoleMsg("playing card...".$data["id_card"]);
               

                     /*--> CALL BACK*/echo json_encode($response);
               


                //taking out energy
               


                //informing enemy
                 }
            }
            
        }
        
    }
    
    function getCardInfo($id_card){
        $sql = "SELECT * from board WHERE id_board = ".$id_card;
        $rec = $this->db->query($sql)->row_array();
        
        return $rec;
    }
    function getCardProperties($num_card){
        $sql = "SELECT * from cards WHERE card_num = ".(int)$num_card;
        $rec = $this->db->query($sql)->row_array();
        
        return $rec;
    }
    
    function move($id_board,$location){
        
        $loc = null;
        switch($location){
            case 0: $loc = "fl"; break;
            case 1: $loc = "tr"; break;
            case 2: $loc = "hl"; break;
            case 3: $loc = "dp"; break;
            case 4: $loc = "dp"; break;
            case 5: $loc = "dp"; break;
            default: $loc = null; break;
        }
        if($loc != null){
            $sql = "UPDATE board SET location ='".$loc."' WHERE id_board =".$id_board;
            $this->db->query($sql);
            
            return true;
            
        }
        
        return false;
    }
    
    
        function consoleMsg($msg){
            if(!empty($msg)){
                
                $sql = "INSERT INTO console(msg) VALUES('".$msg."')";
                $this->db->query($sql);
            }
         }  
}

?>