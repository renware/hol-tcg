<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Game_model extends CI_Model {

    function setQueue($id_user, $token, $date){
        
        $sql = "INSERT INTO finding(id_finder,id_player,found,opp,token,token_rd,stamp) VALUES("
                . $id_user //id_finder
                .",null"  //id_player
                .",false"//found
                .",null"//opp
                .",'".$token."'"//token
                .",null"//token_rd
                .",".$date.");";//stamp
            ;
         $this->db->query($sql);
    }
    
     function onQueue($id_user){
        $sql = "SELECT id_finder FROM finding WHERE id_finder = ".$id_user." LIMIT 1";
        $res = $this->db->query($sql)->row_array();
        
        if($res == null){
            return false;
        }
        return true;
    }
    
     function updateQueue($id_user,$token, $date){

        $res = $this->onQueue($id_user);
        
        if($res == false){
            $this->setQueue($id_user,$token,$date);
        }
        else{
            $update= "UPDATE finding SET token = '".$token."', stamp=".$date." WHERE id_finder =".$id_user;
            $this->db->query($update);

        }
            
    }
    
    
    function stopQueue($id_user){
        $sql = "DELETE FROM finding WHERE id_finder =".$id_user;
        $this->db->query($sql);
        
        return;
    }
    
    function getQueueStatus($id_user){
        $sql = "SELECT * FROM finding WHERE id_finder =".$id_user;
        return $this->db->query($sql)->row_array();
        
    }
    
    function checkQueue($id_user){
       
        $sql = "SELECT * FROM finding
                WHERE id_finder not in(".$id_user.")
                AND found is false
                ORDER BY stamp
                LIMIT 5";
        
        $rec = $this->db->query($sql);
        return $rec->result_array();
                
    }
    
    function attemptMatch($id_user, $player){
        $sql = "UPDATE finding SET id_player =".$id_user." WHERE id_finder =".$player;
        $this->db->query($sql);
    }
    
    
    function checkGame($id_user){
        
       $sql = "SELECT * FROM game
               WHERE player1 = ".$id_user."
               OR player2 = ".$id_user;
               
       
       $rec = $this->db->query($sql);
        
       if(!empty($rec))
           return $rec->row_array();
       
       else
        return false;
       
    }
    
    function createGame($player1,$player2){
        
        if(!empty($player1) && !empty($player2))
        {
        $token = $player1."1aZ92mò-dXX0zx_sakaAáóè".$player2;
        $sql = "INSERT INTO game(active,player1,player2,token) VALUES(1,".$player1.",".$player2.",'".
                md5($token)."')";   
        
        $this->db->query($sql);           
               
        
        $sql="SELECT LAST_INSERT_ID() AS 'id'";
        $res = $this->db->query($sql)->row_array();
            
            $this->stopQueue($player1);
            $this->stopQueue($player2);
            
            return $res;
        }
        return null;
    }
    
    
    function destroyGame($player){
        
        $sql = "DELETE FROM game WHERE player1 = ".$player." OR player2 = ".$player;
        $this->db->query($sql);
        
        $sql = "DELETE FROM board WHERE owner = ".$player;
        $this->db->query($sql);
        
        $sql = "DELETE FROM board_energy WHERE player = ".$player;
        $this->db->query($sql);
        
        
    }
}


?>