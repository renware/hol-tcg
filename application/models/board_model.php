<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_model extends CI_Model {

    
    function getGameData($game){
        $sql = "SELECT * FROM board WHERE id_game = ".$game;
        $data = $this->db->query($sql);
        
        $rec = $data->row_array();
        return $rec;
    }
    function getGameState($game){
        $sql = "SELECT * FROM game_state WHERE id_game = ".$game;
        $data = $this->db->query($sql);
        
        $rec = $data->row_array();
        return $rec;
    }
    function deleteBoardData($game){
        $sql = "DELETE * from board WHERE id_game = ".$game;
        $this->db->query($sql);
    }  
    
    function createBoard($game){
        
        $this->board_model->consoleMsg("CREATING DECKS...");
        
        $sql = "SELECT player1, player2 FROM game WHERE id_game =".$game;
        $res = $this->db->query($sql)->row_array();
        
        $player1 = $res["player1"];
        $player2 = $res["player2"];
        
        
        $this->initialSetup($game);
        $this->createDeck($game,$player1);
        $this->createDeck($game,$player2);
        
        $rand = rand(0,1);
        if($rand == 0){
        $this->createTurns($game,$player1,1);
        $this->createTurns($game,$player2,0);
        
        }
        else{ 
            $this->createTurns($game,$player1,0);
            $this->createTurns($game,$player2,1);
        }
            
        $this->createEnergy($game,$player1);
        $this->createEnergy($game,$player2);    
        //$this->board_model->consoleMsg("PHASE 3...");
        
    }
    function createDeck($game,$player)
    {
        
        $sql = "SELECT card_id, count(card_id) as 'amount' FROM deck WHERE id_collection = ".$player."
                 group by card_id";
        $deck = $this->db->query($sql)->result_array();
        
        $this->consoleMsg("Generating Player ".$player." deck");
        foreach($deck as $card)
        {
            for($a = 0; $a < $card["amount"]; $a++)
            {
            $sql = "INSERT INTO board (id_game,id_card,location,owner) VALUES (".$game.",".$card["card_id"].
                    ",'deck',".$player.")";
            $this->db->query($sql);
            
            }
        }
        
    }
    
    function createTurns($game,$player,$coin){
            $this->consoleMsg("Creating turns...");
            $sql = "INSERT INTO board_turn(id_game,current,player,time_elapsed,time_mark) VALUES ($game, $coin, $player, 0, ".time().")";
            $this->db->query($sql);
            
    } 
    function createEnergy($game,$player){
        $sql = "INSERT INTO board_energy(id_game,player) VALUES (".$game.",".$player.")";
        $this->db->query($sql);
            
    }
    
    function initialSetup($game){
        
        $sql = "SELECT * from game_state WHERE id_game =".$game;
        $res = $this->db->query($sql)->row_array();
        
        $this->consoleMsg("Setting game status:");
        $this->consoleMsg("".var_dump($res));
        
        if(!empty($res["id_game"])){
           $this->consoleMsg("Unknown error");
           return;
        }
        else{
            $this->consoleMsg("Establishing game parameters...");
           
            $sql = "INSERT INTO game_state(id_game,p1_hp,p2_hp,status,token,winner) VALUES(".$game.",40,40,'loading',null,-1)";
            $this->db->query($sql);
        
        }
    }
    function confirmLoad($user,$game){
        $res = $this->getGameState($game);
        
        if($res["status"] == "started"){
            $this->consoleMsg("Player ".$user." has reconnected.");
            return 1;
        }       
        else if($res["status"] == "loading")
        {
            $sql = "UPDATE game_state SET status = 'waiting', winner =".$user." WHERE id_game = ".$game;
            $res = $this->db->query($sql);
            return 0;
        }
        else if($res["status"] == "waiting"){
            $sql = "UPDATE game_state SET status = 'started', winner =-1 WHERE id_game = ".$game;
            $res = $this->db->query($sql);
            return 1;
        }
        else{
            $this->consoleMsg("Error: Couldnt reach loading times... #2");
            return -1;   
        }
    }
    //--fetchers
    
    function fetchName($id_user){
        $sql = "SELECT nick from profile WHERE id_user = ".$id_user;
        return $this->db->query($sql)->row_array();
    }
    
    function fetchRank($id_user){
        $sql = "SELECT rank from profile WHERE id_user = ".$id_user;
        return $this->db->query($sql)->row_array();
    }
    
    function fetchEnergyState($id_user,$id_opp){
        $sql = "SELECT * from board_energy_view WHERE owner =".$id_user;
        $en["my_max"] = $this->db->query($sql)->row_array();
        
        $sql = "SELECT * from board_energy WHERE player = ".$id_user;
        $en["my_used"] = $this->db->query($sql)->row_array();
        
        $sql = "SELECT * from board_energy_view WHERE owner =".$id_opp;
        $en["opp_max"] = $this->db->query($sql)->row_array();
        
        $sql = "SELECT * from board_energy WHERE player = ".$id_opp;
        $en["opp_used"] = $this->db->query($sql)->row_array();
        //$this->consoleMsg(var_dump($en));
        return $en;
    }
    
    function fetchHand($id_user){
        $sql = "SELECT * from board_main_view WHERE owner = ".$id_user." and location = 'hand'";
        $rec = $this->db->query($sql)->result_array();
        
        return $rec; 
    }
    
    function fetchTurnState($id_game,$id_user){
        
        $sql = "SELECT current, player, time_elapsed, time_mark FROM board_turn WHERE player = ".$id_user." and id_game =".$id_game;
        $res["my_turn"] = $this->db->query($sql)->row_array();
        
        
        $sql = "SELECT current, player, time_elapsed, time_mark FROM board_turn WHERE player != ".$id_user." and id_game =".$id_game;
        $res["opp_turn"] = $this->db->query($sql)->row_array();
        
        return $res;
    }
    
    function fetchBoard($id_user,$id_opp){
        $sql = "SELECT * from board_main_view WHERE owner=".$id_user." and location != 'deck'";
        $rec["my_bo"] = $this->db->query($sql)->result_array();
        
        $sql = "SELECT * from board_main_view WHERE owner=".$id_opp." and (location != 'deck' and location != 'hand')";
        $rec["opp_bo"] = $this->db->query($sql)->result_array();
        return $rec;
    }
    
    function fetchMovements($id_game,$start=0) {
        if($start==0)
        {
        $sql = "SELECT * FROM board_movement WHERE id_game =".$id_game;
        $res = $this->db->query($sql);
        }
        else{
             $sql = "SELECT * FROM board_movement WHERE id_game =".$id_game." and id_board_move >=".$start;
            $res = $this->db->query($sql);
        }
        return $res->result_array();  
    }
    
    
    
    function cardDraw($player,$number=1){
        
        for($i = 0; $i < $number; $i++){
            
            //$this->consoleMsg("start draw[".$player."]");
            $sql = "SELECT id_board FROM board WHERE owner = ".$player." and location = 'deck'";
            $deck = $this->db->query($sql)->result_array();
            
            //$this->consoleMsg("offset: ".count($deck));
            
            //$this->consoleMsg("randomizing now...");
            $arr = null;
            foreach($deck as $card){
                $arr[] = $card["id_board"];
            }
            $r = rand(0,count($arr)-1);
            
            //$this->consoleMsg("length: ".count($arr)."- rand: ".$r);
            
            
            $sql = "UPDATE board SET location = 'hand' WHERE id_board = ".$arr[$r];
            $this->db->query($sql);
            
        }
        $this->consoleMsg("draw done[".$number."][".$player."]");
    }
    function cardExchange($player, $index){
        
    }
    
    function cardShuffle($player,$index,$source){}
    function cardDiscard($player,$index){}
    function cardDestroy($player,$index){}
    function cardHandCheck($player,$index){
        
    }
    
    function timerUpdate($player,$offset=0){
        if($offset == 1)
        {
            $sql = "UPDATE board_turn SET time_elapsed = 1 WHERE player=".$player;
            $this->db->query($sql);
        }
        else{
            $sql = "UPDATE board_turn SET time_elapsed = ".time()." WHERE player=".$player;
            $this->db->query($sql);
        }
    }
    function turnUpdate($id_user,$game){

        $sql = "UPDATE board_turn SET current = 0 WHERE player = ".$id_user;
        $this->db->query($sql);
        
        $sql = "UPDATE board_turn SET current = 1 WHERE player != ".$id_user." and id_game=".$game;
        $this->db->query($sql);
    }
    //------console
    
    function consoleMsg($msg){
            if(!empty($msg)){
                
                $sql = "INSERT INTO console(msg) VALUES('".$msg."')";
                $this->db->query($sql);
            }
    }    
    function fetchConsoleMsg($id) {
        $sql = "SELECT * FROM console WHERE id_msg > ".$id;
        $res = $this->db->query($sql);
        
        return $res->result_array();  
    }
    function clearConsole(){
        
        $sql = "DELETE FROM console";
        $this->db->query($sql);
        
    }
}




/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

