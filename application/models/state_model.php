<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class State_model extends CI_Model {

    
    function getProfileState($id_user){
        $sql = "SELECT * FROM profile WHERE id_user = ".$id_user;
        $data = $this->db->query($sql);
        
        $rec = $data->row_array();
        return $rec;
    }
    
    function createProfile($id_user){
        $sql = "SELECT uacc_username FROM user_accounts WHERE uacc_id =".$id_user;
        $rec = $this->db->query($sql)->row_array();
        
        $sql = "INSERT into profile values(".$id_user.",'".$rec["uacc_username"]."','',0,50,0,1,1)";
        $this->db->query($sql);
    }
    
    function newbieGift($id_user,$op){
        
        if($op == "give")
        {
         $sql = "UPDATE profile SET gifts = 1 WHERE id_user = ".$id_user;
         $data = $this->db->query($sql);
         return true;
         
        }
        
        else if($op == "check")
        {
            $sql = "SELECT gifts FROM profile WHERE id_user = ".$id_user;
            $data = $this->db->query($sql);
            
            return $data->row_array();

        }
    }
    
    function markConnection($ip){
        if($this->readByIp($ip)["res"] > 0){
            $sql = "UPDATE user_activity SET stamp = ".time()." WHERE ip = ".$ip;
            $this->db->query($sql);
        }
        else{
            $sql = "INSERT INTO user_activity(ip,stamp) VALUES(".$ip.",".time().")";
            $this->db->query($sql);
        }
    }
       
    function readByIp($ip){
        $sql = "SELECT count(ip) as 'res' from user_activity WHERE ip = ".$ip." LIMIT 1";
        $res = $this->db->query($sql)->row_array();
        return $res;
    }
    
    
    function findActivePeople(){
        $sql = "SELECT count(ip) as 'count' FROM user_activity WHERE stamp >=".(time() - 300);
        return $this->db->query($sql)->row_array();
    }
    
    function findActiveGames(){
        $sql = "SELECT count(id_game) as 'count' FROM game";
        return $this->db->query($sql)->row_array();
    }
}
