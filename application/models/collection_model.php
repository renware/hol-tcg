<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class Collection_model extends CI_Model {

    
    function getCollectionData($id_user)
    {
        $sql = "SELECT c.id_card, c.amount, ca.title FROM collection c JOIN cards ca ON
                (c.id_card = ca.card_num) WHERE id_user =".$id_user." group by ca.title";
        $data = $this->db->query($sql);
        
        $rec = $data->result_array();
        return $rec;
    }

    
    function fetchDeckData($id_user)//, $id_deck=1)
    {//until deck amount is implemented, decks are limited to 1 and the query doesnt resolve itp
        
        $sql = "SELECT d.card_id as 'id', c.title as 'title', count(d.card_id) as 'count'
                FROM deck d
                JOIN cards c
                ON (c.id_card = d.card_id)
                WHERE id_collection = ".$id_user."
                GROUP BY d.card_id, c.title;";
        $data = $this->db->query($sql);
        //$id_deck
        $rec = $data->result_array();
        return $rec;
    }
    
    function getDeckSize($id_user)
    {
        $sql = "SELECT count(id_card) as 'count' FROM collection WHERE id_user = ".$id_user;
        $rec = $this->db->query($sql);
        
        return $rec->row_array();
        
    }
    
    
    function grantCard($id_player, $id_card, $amount=1)
    {
        $sql = "SELECT amount FROM collection WHERE id_user=".$id_player." and id_card=".$id_card;
        $res = $this->db->query($sql)->row_array();

        
        if(empty($res["amount"]))
        {
            $sql = "INSERT INTO collection (id_user, id_card, amount) VALUES (".$id_player.",".$id_card.",".$amount.")";
            $this->db->query($sql);
            return;
        }
       
        else
        {
            $sql = "UPDATE collection SET amount = amount +".$amount." WHERE id_user=".$id_player." and id_card =".$id_card;
            $this->db->query($sql);
            return;
        }
    }
    
    function addToDeck($player,$id_card,$amount){
        
        for($i=0;$i<$amount;$i++){
        $sql = "INSERT INTO deck(id_deck,id_collection,card_id) VALUES(1,".$player.",".$id_card.")";
        $this->db->query($sql);
        
        }
    }
    
    
    function removeCard($id_player, $id_card){
        
        $sql = "UPDATE collection SET amount = amount - 1 WHERE id_user = ".$id_player." and id_card=".$id_card;
        $this->db->query($sql);
        
    }
    
    function checkCollectionGift($id_player,$type){
        
        $sql = "SELECT gifts FROM profile WHERE id_user =".$id_player;
        $chk = $this->db->query($sql)->row_array();
        
 
        
        if($chk["gifts"] < 1)
        {
            
            $sql = "UPDATE profile SET gifts = 1 WHERE id_user=".$id_player;
            $this->db->query($sql);
            
            if($type == 1){
            $this->grantCard($id_player, 1,2);
            $this->grantCard($id_player, 2,2);
            $this->grantCard($id_player, 3,2);
            $this->grantCard($id_player, 4,2);
            $this->grantCard($id_player, 5,2);
            $this->grantCard($id_player, 6,2);
            $this->grantCard($id_player, 7,2);
            $this->grantCard($id_player, 29,2);
            $this->grantCard($id_player, 30,2);
            $this->grantCard($id_player, 64,2);
            $this->grantCard($id_player, 65,2);
            
            $this->addToDeck($id_player,1,2);
            $this->addToDeck($id_player,2,2);
            $this->addToDeck($id_player,3,2);
            $this->addToDeck($id_player,4,2);
            $this->addToDeck($id_player,5,2);
            $this->addToDeck($id_player,6,2);
            $this->addToDeck($id_player,7,2);
            $this->addToDeck($id_player,29,2);
            $this->addToDeck($id_player,30,2);
            $this->addToDeck($id_player,64,2);
            $this->addToDeck($id_player,65,2);
            
            
            return true;
            }
            
            else
            {

           
            $this->grantCard($id_player, 6,2);
            $this->grantCard($id_player, 7,2);
            $this->grantCard($id_player, 29,2);
            $this->grantCard($id_player, 30,2);
            $this->grantCard($id_player, 64,2);
            $this->grantCard($id_player, 65,2);
            $this->grantCard($id_player, 68,2);
            $this->grantCard($id_player, 69,2);
            $this->grantCard($id_player, 45,2);
            $this->grantCard($id_player, 8,2);
            $this->grantCard($id_player, 11,2);
            
            $this->addToDeck($id_player,6,2);
            $this->addToDeck($id_player,7,2);
            $this->addToDeck($id_player,29,2);
            $this->addToDeck($id_player,30,2);
            $this->addToDeck($id_player,64,2);
            $this->addToDeck($id_player,65,2);
            $this->addToDeck($id_player,68,2);
            $this->addToDeck($id_player,69,2);
            $this->addToDeck($id_player,45,2);
            $this->addToDeck($id_player,8,2);
            $this->addToDeck($id_player,11,2);
            
            return true;
            }
        }
        
        else
            return false;
    }
    
    
}




?>