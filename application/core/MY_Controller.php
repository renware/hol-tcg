<?php defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller 
{
	function __construct($auth=false) 
        {
            parent::__construct();
		
		// Load CI benchmark and memory usage profiler.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => true, 'controller_info' => true, 'get' => true, 'post' => true, 'queries' => true, 
				'uri_string' => true, 'http_headers' => true, 'session_data' => true
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		if(!isset($this->auth))
                    $this->auth = new stdClass;
		
		// Load 'lite' flexi auth library by default.
		// If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
		// This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
                if($auth)
                    $this->load->library('flexi_auth');
                else
                    $this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');	

		
		$this->data = null;
                setlocale(LC_ALL, 'es_CL');
                /*
                if (!$this->flexi_auth->is_logged_in() && uri_string() != 'auth/logout') 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('Debe iniciar sesión para ingresar a esta área.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('auth');
		}*/
                
        }
        
         public function format_money($value=false,$row=false)
        {   
            return money_format('%.0n', $value);  
        }
        
        public function error_messages($mensaje=false)
        {
            $this->output->set_common_meta('Messages', 'Alert', '');
            
            $this->_init("aero",true);
            $data['message']=$mensaje;
            $this->load->view("public/error_message",$data);
        }
        
        public function _init($template='aero',$jQuery=false)
	{
            $this->output->set_template($template);
            $this->load->css(site_url('assets/themes/aero/css/plugins/pace/pace.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/bootstrap/css/bootstrap.min.css'));
            $this->load->css(site_url('assets/themes/aero/icons/font-awesome/css/font-awesome.min.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/messenger/messenger.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/messenger/messenger-theme-flat.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/daterangepicker/daterangepicker-bs3.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/morris/morris.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/jvectormap/jquery-jvectormap-1.2.2.css'));
            $this->load->css(site_url('assets/themes/aero/css/plugins/datatables/datatables.css'));
            $this->load->css(site_url('assets/themes/aero/css/style.css'));
            $this->load->css(site_url('assets/themes/aero/css/demo.css'));
            
            if($jQuery)
                $this->load->js('assets/themes/aero/js/jquery.min.js');
            
            $this->load->js(site_url('assets/themes/aero/js/plugins/pace/pace.js'));
            
            $this->load->js('assets/themes/aero/js/global.js');
            $this->load->js(site_url('assets/themes/aero/js/plugins/bootstrap/bootstrap.min.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/slimscroll/jquery.slimscroll.min.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/popupoverlay/jquery.popupoverlay.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/popupoverlay/defaults.js'));
           
            $this->load->js(site_url('assets/themes/aero/js/plugins/hisrc/hisrc.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/messenger/messenger.min.js'));
            $this->load->js(site_url('assets/themes/aero/js/plugins/messenger/messenger-theme-flat.js'));
            
            
            $this->load->js(site_url('assets/themes/aero/js/flex.js'));      
            #$this->load->js(site_url('assets/themes/aero/js/demo/dashboard-demo.js')); 
	}
        
        private function __validate_user()
    {
        $this->output->unset_template();
        $id_user = $this->input->post("id_user");
        $token=$this->input->post("token");
        return true;
    }
}