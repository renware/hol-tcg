<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board extends CI_Controller {

       function __construct() 
       {

        parent::__construct();

		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/flexi_auth/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
                
                if($this->input->is_ajax_request()){
                    if ($this->flexi_auth->is_logged_in()) 
                    {
                        $this->load->model("board_model");
                        $this->load->model("game_model");
                        $this->load->model("action_model");

                    }
                    else
                     exit('Disconnected');
                }
                else
                    if ($this->flexi_auth->is_logged_in()) 
                    {
                        $this->load->model("board_model");
                        $this->load->model("game_model");
                        $this->load->model("action_model");

                    }
                    else
                     redirect('home');
                    
	}
        
        
	function index()
        { 
             $id_user = $this->flexi_auth->get_user_id();
             
             if($this->game_model->checkGame($id_user) != false)
             $this->startingBuild();
             
             else
                 redirect('game');
	}
        
        /*game init*/
        
        function startingBuild(){
            //create basic environment for players
          if ($this->flexi_auth->is_logged_in()) 
          {
               $id_user = $this->flexi_auth->get_user_id();
              
               
               $game = $this->game_model->checkGame($id_user);
               $this->board_model->confirmLoad($id_user,$game["id_game"]);
               //$this->board_model->consoleMsg("Waiting confirmation [player: [".$id_user."]");
               $this->load->view("maquetado");
               $this->load->view("engine");
          }
            else{
            redirect('home');
            }
            
        }    
            
        
        
        
        /* game feed */
        function boardRefresh(){
                $id_user = $this->flexi_auth->get_user_id();
                
                $game = $this->game_model->checkGame($id_user);
                       
                if($game["player1"] == $id_user)
                $id_opp = $game["player2"];
                else
                $id_opp = $game["player1"];

                $bo = $this->board_model->fetchBoard($id_user,$id_opp);
                 echo json_encode($bo);
        }      //loads manually
        function createInfoFeed(){
            $id_user = $this->flexi_auth->get_user_id();
            
            
            $game = $this->game_model->checkGame($id_user);
            $state = $this->board_model->getGameState($game["id_game"]);
            $data["self_rank"] = $this->board_model->fetchRank($id_user);
            
            if($game["player1"] == $id_user){
                $data["self"] = $this->board_model->fetchName($game["player1"])["nick"];
                $data["opp"] = $this->board_model->fetchName($game["player2"])["nick"];
                
                $data["opp_rank"] = $this->board_model->fetchRank($game["player2"]);
                $data["self_hp"] = $state["p1_hp"];
                $data["opp_hp"] = $state["p2_hp"];
            }
            else
            {
                $data["self"] = $this->board_model->fetchName($game["player2"])["nick"];
                $data["opp"] = $this->board_model->fetchName($game["player1"])["nick"];
                $data["self_hp"] = $state["p1_hp"];
                $data["opp_hp"] = $state["p2_hp"];
                $data["opp_rank"] = $this->board_model->fetchRank($game["player1"]);
            }
            
            echo json_encode($data);
            //
            //retrieve your hand cards/deck count
            //retrieve enemy counters (hand/deck)
            
            
        }    //loads once only
        function createEnergyFeed(){
            
            $id_user = $this->flexi_auth->get_user_id();
            
            $game = $this->game_model->checkGame($id_user);
                       
            if($game["player1"] == $id_user)
            $id_opp = $game["player2"];
            else
            $id_opp = $game["player1"];
            
            $en = $this->board_model->fetchEnergyState($id_user,$id_opp);
            echo json_encode($en);
        }  //loads manually
        function createHandFeed(){
              
            $id_user = $this->flexi_auth->get_user_id();
            
            
            $ca = $this->board_model->fetchHand($id_user);
            echo json_encode($ca);
        }     //loads manually   
        function movementRefresh(){
            $id_user = $this->flexi_auth->get_user_id();
            $game = $this->game_model->checkGame($id_user);
            
            $st = 0;
            if($this->input->post("start",true)){
                $st = (int)$this->input->post("start");
            }
            
            
            $mov = $this->board_model->fetchMovements($game["id_game"],$st);
            echo json_encode($mov);
        }                //loads auto
        
        
        
        /*game conditions*/
        function checkBurnout($player){}
        function checkGameOver(){
        }
        function checkGameState(){                  //loads everysec
            $id_user = $this->flexi_auth->get_user_id();
            
            $game = $this->game_model->checkGame($id_user);
            $state = $this->board_model->getGameState($game["id_game"]);

            if($state["status"] == 'started')
            {
               $turns = $this->board_model->fetchTurnState($game["id_game"],$id_user);
               
               if($turns["my_turn"]["time_elapsed"] == 0){
                   $this->drawHand($id_user, $turns["my_turn"]["current"]);
                   $this->toggleTimer(1);

               }
              
               else if($turns["my_turn"]["time_elapsed"] != 1 && $turns["my_turn"]["current"] == 1)
               {
                   echo "my_turn";
               }
               
               else if($turns["my_turn"]["time_elapsed"] != 1 && $turns["my_turn"]["current"] == 0)
               {
                   echo "opp_turn";
               }
               
               else if($turns["my_turn"]["time_elapsed"] == 1)
                echo "choose_hand";
               
               else
                   echo "666";
            }
            else if($state["status"] == finished){
               $this->endGame($id_user,$state["status"]);
            }
            
            
        }
        
        
         /*game development*/
        
        function drawHand($id_user, $current){
            if($id_user == $current)
            {
                $this->board_model->cardDraw($id_user,5);
                $this->toggleTimer($id_user,1);
                
            }
            else{
                $this->board_model->cardDraw($id_user,6);
                $this->toggleTimer($id_user,1);
            }
                
        }
        function cardExchange(){
        
            $id_user = $this->flexi_auth->get_user_id();
            
            if($this->input->post('id')){
                $card = (int)$this->input->post('id');
                $this->board_model->cardShuffle($id_user,$card);
                $this->board_model->cardDraw(1);
                
            }
        }
        function surrender(){
            //temporarily destroys the game
            if ($this->flexi_auth->is_logged_in()) 
            {
                   $id_user = $this->flexi_auth->get_user_id();
            
            
                $this->game_model->destroyGame($id_user);
                redirect('game');
            
            }
            
        }   
        
        function playCard()
        {
            $id_user = $this->flexi_auth->get_user_id();
            
            if($this->input->post("id")){
                $card = $this->input->post("id");
                $solve  = $this->action_model->play($id_user,$card);
                
                echo $solve;
            }
            else
                return null;
        }
        
        /*- game triggers-*/
        
        
        function endTurn(){
            $id_user = $this->flexi_auth->get_user_id();
            $game = $this->game_model->checkGame($id_user);
            
            $turnstate = $this->board_model->fetchTurnState($game["id_game"],$id_user);
            
            if($turnstate["my_turn"]["current"] == 1){
                $this->board_model->turnUpdate($id_user,$game["id_game"]);
                echo 'turn_ended';
            }
            
            else
                echo 'opponent_turn';
            
        }
        function endGame($id_user,$status){
        
             if($status == -1)
                echo "game_finished_draw";
                
                else if($status == $id_user)
                echo "game_finished_win";
                
                else
                echo "game_finished_lose";
            
        }
        function toggleTimer($value=0){
       
            $id_user = $this->flexi_auth->get_user_id();
            
            if($value == 1) //if hand its confirmed
                $this->board_model->timerUpdate($id_user,1);
            
            else
                $this->board_model->timerUpdate($id_user);
        }

        /* console methods */
        
        function refreshConsole(){
            
            if($this->input->post("id") == null)
            echo 'Null id.';
            
            
            
            else
            {
                $id = $this->input->post("id");
                
                $this->load->model("board_model");
                $lines = $this->board_model->fetchConsoleMsg($id);
                
                echo json_encode($lines);
               
            }

        }     
        function clearConsole(){
            $this->load->model("board_model");
            $this->board_model->clearConsole();
        }   
        function consoleCallback(){
            
        }
        
        
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>