<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game extends CI_Controller {
 
     function __construct() 
     {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			/*$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			);*/ 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		$this->load->model('collection_model');
                $this->load->model("game_model");
		
	}
   
    
    function index()   {
          if ($this->flexi_auth->is_logged_in()) 
          {
               $id_user = $this->flexi_auth->get_user_id();
              
               $this->load->model("game_model");
               $this->findStop($id_user);
               
              if($this->game_model->checkGame($id_user) != false){
                $this->play();
              }
              else
                  $this->load->view("game_view");
              
          }
            else
                redirect('home');
            
         
    }
    function play(){
          if ($this->flexi_auth->is_logged_in()) 
          {
              redirect('board');
          }
            else
                $this->load->view('demo/login_view');
        
        
    }  
    
    function find(){
   
        if ($this->flexi_auth->is_logged_in())
        {

            
            $id_user = $this->flexi_auth->get_user_id();
            
            //checkgame for refresh
            if($this->game_model->checkGame($id_user)!=false){
                echo json_encode("FOUND");
            }
            
            
            //check if this player is on queue
            if($this->game_model->onQueue($id_user))
            {
                 //if it this, download the current status
                $queue = $this->game_model->getQueueStatus($id_user);
                
                //check if the player already searched for an opponent
                if(!empty($queue) && $queue["id_player"] != null)
                {
                    $opponentstatus = $this->game_model->getQueueStatus($queue["id_player"]);
    
                    //if the opponent chose this user
                    if($opponentstatus["id_player"] == $id_user){
                        //START GAME
                        //MATCH ALREADY ACCEPTED
                        $gameid = $this->game_model->createGame($id_user,$opponentstatus["id_finder"]);  
                        
                        $this->load->model("board_model");
                        $this->board_model->consoleMsg("GAME CREATED... FETCHING DATA:~");
                        $this->board_model->consoleMsg("GAME ID :".$gameid["id"]);
                        $this->board_model->consoleMsg("PLAYER 1: ".$id_user);
                        $this->board_model->consoleMsg("PLAYER 2: ".$opponentstatus["id_finder"]);
                        
                        $this->board_model->createBoard($gameid["id"]);
                        
                        echo json_encode("FOUND");
                        
                    }
                    
                    //if the opponent still didnt but its blank
                    else if($opponentstatus["id_player"] == null){
                        //retry
                        $this->game_model->attemptMatch($id_user,$opponentstatus["id_finder"]);
                        echo json_encode("WAITING");
                    }
                    
                    
                 }

            }
              //if fails everything at current queue
                   $players = $this->game_model->checkQueue($id_user);
                   
                   //attempt match with latest players on pool
                   
                    foreach($players as $player){
                        if($player["id_player"] == null){
                            $this->game_model->attemptMatch($id_user,$player["id_finder"]);
                        }
                    }
        
                    $data["date"] = (int)time();
                    $data["key"] = md5($data["date"]+"connection"+$id_user);


                    $this->game_model->updateQueue($id_user,$data["key"],$data["date"]);

            
        }//if not logged
        
        else
            redirect('home');
       
    }
    function findStop()
    {
        if ($this->flexi_auth->is_logged_in())
        {
            $this->load->model("game_model");
            $id_user = $this->flexi_auth->get_user_id();
            
            $this->game_model->stopQueue($id_user);
        }
        
    }
    
    
    function validateDeck(){
        
        if ($this->flexi_auth->is_logged_in())
        {
            $id_user = $this->flexi_auth->get_user_id();
            $decksize = $this->collection_model->getDeckSize($id_user);
            
            
            if(empty($decksize) || ($decksize["count"] <= 5)){
               echo json_encode(false);
            }
            
            else
               echo json_encode(true);
        }
    }    
    function checkActiveGame(){
        
        if ($this->flexi_auth->is_logged_in())
        {
            $id_user = $this->flexi_auth->get_user_id();
            
            if($this->game_model->checkGame($this->flexi_auth->get_user_id()) != false){
               echo json_encode();
            }
        }
        
    }
    
    function connect(){
        
        if ($this->flexi_auth->is_logged_in())
        {
            $id_user = $this->flexi_auth->get_user_id();
        }
        
    }
}