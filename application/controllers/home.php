<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {


    
       function __construct() 
       {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');
                $this->load->model("state_model");

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		
		
	}
        
        
       public function index(){
    
            $stats = $this->mark_conn();
            
            if ($this->flexi_auth->is_logged_in()) 
            {
			$this->load->view('home_view',$stats);
            }
            else{
                redirect();
            }   
	}
        
       public function mark_conn(){
            
            if (!empty($_SERVER['HTTP_CLIENT_IP'])){
                $ip=$_SERVER['HTTP_CLIENT_IP'];
            }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; 
            }else{
             $ip=$_SERVER['REMOTE_ADDR'];
            }
            
            $ip = ip2long($ip);
            
            if($ip == 0)
            {
                $ipParts = explode(":", $ip);
                
                for($i=0;$i<=7;$i++)
                    $ip += hexdec($ipParts[$i]);
            
            }
            
            $this->state_model->markConnection($ip);
            
            if($this->input->post("js",true))
            echo json_encode($this->retrieveStats());
                    
            else
            return $this->retrieveStats();
            
            
        }
            
       public function retrieveStats(){
           
           $val = array();
           
           $val["p"] = $this->state_model->findActivePeople()["count"];
           $val["g"] = $this->state_model->findActiveGames()["count"];
           
           return $val;
       }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */