<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Profile extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
       function __construct() 
       {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
                $this->load->library('grocery_CRUD');
		
	}
        
        
	public function index($msg="")
	{
            $this->load->vars('base_url', 'http://localhost/');
            $this->load->helper('url');
           
            
            if ($this->flexi_auth->is_logged_in()) 
            {          
                        $this->load->model('state_model');
                        
                        $data["profile"] = $this->state_model->getProfileState($this->flexi_auth->get_user_id());
                        
                        if(!empty($msg))
                            $data["msg"] = $msg;
                        
			$this->load->view('profile_view',$data);
            }
            else
                redirect('home');
            
	}
        
        public function newbieGift()
        {
            if ($this->flexi_auth->is_logged_in()) 
            { 
                $id_user = $this->flexi_auth->get_user_id();
            
            
                $this->load->model("collection_model");

                $res = $this->collection_model->checkCollectionGift($id_user,1);

                if($res == true)
                   $msg = "Your initial set of cards has been granted!";

                else
                   $msg = "Your initial set of cards has been already given";

            $this->index($msg);
            
            }
            else
              redirect('home');
        }
        
        public function newbieGift2()
        {
            if ($this->flexi_auth->is_logged_in()) 
            { 
                $id_user = $this->flexi_auth->get_user_id();
            
            
                $this->load->model("collection_model");

                $res = $this->collection_model->checkCollectionGift($id_user,2);

                if($res == true)
                   $msg = "Your initial set of cards has been granted!";

                else
                   $msg = "Your initial set of cards has been already given";

            $this->index($msg);
            
            }
            else
              redirect('home');
        }
        
        
        
        
}