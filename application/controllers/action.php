<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Action extends CI_Controller {

       function __construct() 
       {

        parent::__construct();

		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/flexi_auth/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		
                if ($this->flexi_auth->is_logged_in()) 
                {
                    $this->load->model("board_model");
                    $this->load->model("game_model");
                    $this->load->model("action_model");
                     
                }
                else
                 redirect('home');
	}
        
        
	function index()
        { 
             $id_user = $this->flexi_auth->get_user_id();
             
             if($this->game_model->checkGame($id_user) != false)
             $this->startingBuild();
             
             else
                 redirect('game');
	
             
        }


        
}
        