<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Collection extends CI_Controller {

    
       function __construct() 
       {
        parent::__construct();

        
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/flexi_auth/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		
		
	}
        
        
	public function index()
	{
            $this->load->vars('base_url', 'http://localhost/');
            $this->load->helper('url');
           
            
            if ($this->flexi_auth->is_logged_in()) 
            {
                     $this->load->model('collection_model');
                     $data["collection"] = $this->collection_model->getCollectionData($this->flexi_auth->get_user_id());
                     $data["deck"] = $this->collection_model->fetchDeckData($this->flexi_auth->get_user_id(),1);
                     $this->load->view('collection_view_2',$data);    
            }
            else
                redirect('home');
            
	}
        
        
       
        
       


}
