<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
       function __construct() 
       {
        parent::__construct();
		
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');
                
                $this->load->vars('base_url', 'http://localhost/');
		$this->load->vars('includes_dir', 'http://localhost/flexi_auth/includes/');
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
                $this->load->library('grocery_CRUD');
		
       
        
	}
        
        
        
	public function index()
	{
            $this->load->vars('base_url', 'http://localhost/');
            $this->load->helper('url');
           
            
            if ($this->flexi_auth->is_logged_in()) 
            {
			$this->load->view('admin_view');
            }
            else
                redirect('home');
            
	}
        
        
        public function cards()
        {
            try{
			$crud = new grocery_CRUD();

			//$crud->set_theme('gr');
			$crud->set_table('cards');
			$crud->set_subject('Cards');
			$crud->required_fields('card_id');
			$crud->columns('card_id','card_num','card_img','title','type','atk','def');

			$output = $crud->render();

			$this->load->view('admin_view',$output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
            
            
        }
        
        
        public function users()
        {
            try{
			$crud = new grocery_CRUD();
                        $crud->unset_jquery();
			$crud->set_theme('datatables');
			$crud->set_table('profile');
			$crud->set_subject('Users');
			$crud->required_fields('id_user');
			

			$output = $crud->render();

			$this->load->view('admin_view',$output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
            
            
        }
        
         public function collections()
        {
            try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('collection');
			$crud->set_subject('Collections');
			$crud->required_fields('id_collection');
			$crud->columns('id_collection','id_user','id_card','amount');

			$output = $crud->render();

			$this->load->view('admin_view',$output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
            
            
        }
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */