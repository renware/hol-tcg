<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Profile</title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/> 
	<meta name="keywords" content="demo, flexi auth, user authentication, codeigniter"/>
	<?php $this->load->view('includes/head'); ?> 
</head>

<body id="public_dashboard">

<div id="body_wrap">
	<!-- Header -->  
	<?php $this->load->view('includes/header'); ?> 

	<!-- Demo Navigation -->
	<?php $this->load->view('includes/demo_header'); ?> 
	
	<!-- Intro Content -->
     
	
	<!-- Main Content -->
	
        
	<div class="content_wrap main_content_bg">
            <?php
            if(!empty($msg)){
                ?>
                <div class="content clearfix notify">
                <?php echo $msg;?>
                </div>
            <?php
            }
            ?>
            
            <div class="content clearfix">
                <div class="div-small-bordered-20">
                    <div class="nick-title">
                        <?php echo $profile["nick"];?>
                    </div>
                    <div class="av">
                        
                        <img src="<?php echo base_url();?>assets/img/av/1.png"/>
                    </div>
                    <div class="div-noborder-100">
                        <a href="#">Change Avatar</a>
                    </div> 
                </div>   
                <div class="div-small-bordered-60">
                    <div class="div-noborder-100">
                        <div class="title">
                            My Statistics
                        </div>
                    </div>
                    
                    
                    <div class="div-noborder-100">
                        <div class="profile-icon">LEVEL <?php echo $profile["lv"];?></div>
                        <div class="profile-data">
                          
                                <div id="progressbar">
                                     <div>  <?php //echo $profile["exp"];?></div>
                                  </div>

  
                        </div>
                    </div> 
                    <div class="div-noborder-100">
                        <div class="profile-icon">
                            <img src="<?php echo base_url();?>assets/img/rank/<?php echo $profile["rank"];?>.png"/></div>
                        <div class="profile-data">
                        <?php
                        switch($profile["rank"]){
                            case 1:  echo "RANK 1 - Apprentice";
                                     break;
                                 
                            case 2:  echo "RANK 2 - Magician";
                                     break;
                                 
                            case 3:  echo "RANK 3 - Wizard";
                                     break;
                                 
                            case 4:  echo "RANK 4 - High Wizard";
                                     break;
                                 
                            case 5:  echo "RANK 5 - Ancestral Wizard";
                                     break;
                                 
                            default: echo "...";
                                     break;
                        }?>
                        </div>
                    </div> 
                    <div class="div-noborder-100">
                        <div class="profile-icon">
                        <img src="<?php echo base_url();?>assets/img/yen_coin.png"/>
                        </div>
                        <div class="profile-data"><?php echo $profile["cash"];?></div>
                    </div> 
                    
                    <div class="div-noborder-100">
                        <div class="profile-icon">Inventory</div>
                        <div class="profile-data">
                            <a href="<?php echo base_url();?>profile/newbieGift">
                                <img src="<?php echo base_url();?>assets/img/buttons/newbiegift.png"/>
                            </a>
                            <a href="<?php echo base_url();?>profile/newbieGift2">
                                <img src="<?php echo base_url();?>assets/img/buttons/newbiegift2.png"/>
                            </a>
                        </div>
                    </div> 
               
                    <hr/>
               
                </div>
            </div>
        </div>


        <!-- Footer -->  
	<?php $this->load->view('includes/footer'); ?> 


</div>	
	
	
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 

</body>
</html>