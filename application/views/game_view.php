<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Free Trading Card Game</title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/> 
	<meta name="keywords" content="flexi auth, user authentication, codeigniter"/>
	<?php $this->load->view('includes/head'); ?> 
</head>

<body>

    <div id="body_wrap">
            <!-- Header -->  
            <?php $this->load->view('includes/header'); ?> 

            <!-- Demo Navigation -->
            <?php $this->load->view('includes/demo_header'); ?> 


            <!-- Intro Content -->
            <div class="content_wrap intro_bg">

            </div>
            <!-- Main Content -->
            <div class="content_wrap main_content_bg">
                <div class="content clearfix">
                
                <div class="div-small-bordered-40">
                    <div class="div-noborder-100">
                        <div class="title">Start Battle!</div>
                        Find an Opponent to battle online!
                        <a id="play" href="#">
                            <img src="<?php echo base_url();?>assets/img/buttons/find.png" alt="Start game against another player!"/>
                        </a>
                        <a id="stop" href="#" style="display: none;">
                            <img src="<?php echo base_url();?>assets/img/buttons/stop.png" alt="Cancel"/>
                        </a>
                    </div>
                    <div id="loading" class="div-noborder-100" style="display: none;">
                        <span id="message"></span>
                        <span id="timer"></span>
                    </div>
                </div>
                    
                <div class="div-small-bordered-40">
                    <div class="div-noborder-100">
                     <div class="title">Adventure</div>
                    Battle against the computer to earn special rewards and prices!
                    
                   
                        <a href="game/adventure">Coming Soon</a>
                     </div>
                </div>
                </div>
            </div>	

    </div>

<!-- Footer -->  
<?php $this->load->view('includes/footer'); ?> 

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 

</body>
</html> 

<script>
function get_elapsed_time_string(total_seconds) {
    
    function pretty_time_string(num) {
    return ( num < 10 ? "0" : "" ) + num;
    }

    var hours = Math.floor(total_seconds / 3600);
    total_seconds = total_seconds % 3600;

    var minutes = Math.floor(total_seconds / 60);
    total_seconds = total_seconds % 60;

    var seconds = Math.floor(total_seconds);

    // Pad the minutes and seconds with leading zeros, if required
    hours = pretty_time_string(hours);
    minutes = pretty_time_string(minutes);
    seconds = pretty_time_string(seconds);

    // Compose the string for display
    var currentTimeString = hours + ":" + minutes + ":" + seconds;

    return currentTimeString;
}    
    

$("#play").click(function()
{
    $.getJSON(window.location.origin +"/game/validateDeck", function( response ) {
        if(response === false || response == null){
            $("#message").html("Can't find game due to incomplete deck. ");
            $("#loading").show();
        }
        
        else{
 
        $("#message").html("Finding Match");
        $("#loading").slideToggle("fast");
        $("#play").slideToggle("fast");
        $("#stop").slideToggle("fast");

        var start = new Date;

        timer = setInterval(function() {

            var current = Math.round((new Date - start) / 1000, 0);
             $("#timer").html(get_elapsed_time_string(current));


            if((current % 5) === 0)
                $.getJSON(window.location.origin +"/game/find", function( response ) {
                    
                    if(response === 'FOUND'){
                      window.location.reload();
                    }
                    
                    else if(response === 'WAITING')
                        $("#message").html("Opponent Found! please wait...");
                    
                    else if(response === false)
                        console.log("null response");
                   
                    else
                    {
                        console.log(response);
                    }
                });


        }, 1000);
        
        }
        
    });
});

$("#stop").click(function()
{
    clearInterval(timer);
    $("#message").html("STOPPING GAME...");
    $.getJSON(window.location.origin +"/game/findStop", function( response ){});
    $("#loading").slideToggle("fast");
    $("#play").slideToggle("fast");
    $("#stop").slideToggle("fast");   
});
    

    
/*
  var elapsed_seconds = 0;
  setInterval(function() {
    elapsed_seconds = elapsed_seconds + 1;
    $('#box_header').text(get_elapsed_time_string(elapsed_seconds));
  }, 1000);
   
});*/ 
</script>