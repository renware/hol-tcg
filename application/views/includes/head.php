	<meta name="robots" content="index, follow"/>
	<meta name="designer" content="haseydesign - Rob Hussey : rob @ haseydesign .com"/> 
	<meta name="copyright" content="Copyright <?php echo date('Y');?> Rob Hussey, All Copyrights Reserved"/>
	<meta http-equiv="imagetoolbar" content="no"/>	
	
	<link rel="stylesheet" href="<?php echo $includes_dir;?>css/global.css?v=1.0">
	<link rel="stylesheet" href="<?php echo $includes_dir;?>css/structure.css?v=1.0">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/fstyle.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/themes/gr/css/plugins/datatables/datatables.css">
         <link rel="stylesheet" href="<?php echo base_url();?>assets/themes/gr/css/plugins/bootstrap/css/bootstrap.min.css">
         