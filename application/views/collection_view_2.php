<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Home</title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/> 
	<meta name="keywords" content="demo, flexi auth, user authentication, codeigniter"/>
	<script src="<?php echo base_url();?>assets/js/external/jquery/jquery.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.animsition.min.js"></script>
            <?php $this->load->view('includes/head'); ?> 
        
        <link type="text/css" href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui.css">
       
        
</head>

  <script>
  $(function() {
    // there's the gallery and the trash
    var $gallery = $( "#gallery" ),
      $trash = $( "#trash" );
 
    // let the gallery items be draggable
    $( "li", $gallery ).draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
 
    // let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: "#gallery > li",
      helper: "clone",
      //activeClass: "ui-state-highlight",
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: "#trash li",
      //activeClass: "custom-state-active",
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // image deletion function
    var recycle_icon = "<a href='#' title='Remove' class='ui-icon ui-icon-refresh'>Remove</a>";
    function deleteImage( $item ) {
      
      $cantidad = $item.find("span.card-remaining").html();
      $item.find("span.card-remaining").html($cantidad - 1);
        
      
        var $list = $( "ul", $trash ).length ?
          $( "ul", $trash ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo($trash);
        var $new = $item;
        
        $new.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $new
            .animate({ width: "220px", height: "25px"})
            .css("background-size", "220px 30px;")
            .css("background-position","-20px -45px");
            
             //.find( "img" )
              //.animate({ height: "30px" });
              
        });
     
    }
 
    // image recycle function
    var trash_icon = "<a href='#' title='Add to deck' class='ui-icon ui-icon-trash'>Add to deck</a>";
    function recycleImage( $item ) {
      
      $item.fadeOut(function() {
        $item
          .find( "a.ui-icon-refresh" )
            .remove()
          .end()
          .css( "width", "96px")
          .css( "height", "123px")
          .css("background-position","0 0")
          .append( trash_icon )
          .find( "img" )
            .css( "height", "120px" )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
        //console.log($link);
        var src = $link.css( "background-image" ),
        title = $link.attr("title"),
        $modal = $( "img[src$='" + src + "']" );
 
      if ( $modal.length ) {
        $modal.dialog( "open" );
      } else {
        var img = $( '<div'+ " alt='" + title + "'/>" )
                .css( "background-image", src )
                .css( "background-repeat", "no-repeat")
                .css( "width","240px")
                .css( "height","500px")
          .appendTo( "body" );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            height: 550,
            modal: true
          });
        }, 1 );
      }
    }
 
    // resolve the icons behavior with event delegation
    $( "ul.gallery > li" ).click(function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( "a.ui-icon-trash" ) ) {
        deleteImage( $item );
      } else if ( $target.is( "a.ui-icon-refresh" ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
    
    $("ul.gallery > li").on('contextmenu', function(evt)
    {
        var $item = $(this);
        viewLargerImage($item);
        evt.preventDefault();
    });
    
    /*
    $("body").on('contextmenu', function(evt)
    {
        evt.preventDefault();
    });
    */
    
    
    
    
    
    
  });
  </script>
</head>

<body id="public_dashboard">

<div id="body_wrap">
	<!-- Header -->  
	<?php $this->load->view('includes/header'); ?> 

	<!-- Demo Navigation -->
	<?php $this->load->view('includes/demo_header'); ?> 
	
	<!-- Intro Content -->
        <div class="content_wrap main_content_bg">
            <div class="msg">WARNING: SECTION UNDER CONSTRUCTION
              <br>
              Press right click to zoom-in.
            </div>
        <div class="ui-widget ui-helper-clearfix">

        <ul id="gallery" class="gallery ui-helper-reset ui-helper-clearfix">
           <?php
                        $index = 0;
                        foreach($collection as $card)
                        {
                        $index++;
                        ?>
                        <li title="<?php echo $card["title"];?>" class="ui-widget-content ui-corner-tr" style="background-image: url(<?php echo base_url();?>assets/img/cards/<?php echo $card["id_card"];?>.png); background-size: cover;">
                            <span class="card-remaining">
                               <?=$card["amount"];?>
                            </span>
                            <span class="slash">
                               /
                            </span>
                            <span class="card-total">
                                <?=$card["amount"];?>
                            </span>
                        </li>

                        <?php
                        }
            ?>

        </ul>

        <div id="trash" class="ui-widget-content ui-state-default">
          <h4 class="ui-widget-header">
              <a href="#">
                  <span class="ui-icon ui-icon-trash">Save Deck</span>Save Deck
          </h4>
                  
              
          
        </div>

        </div>
            
        </div>

    
 
	<!-- Footer -->  
	<?php $this->load->view('includes/footer'); ?> 
</div>

</body>
</html>