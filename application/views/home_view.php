<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Home</title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/> 
	<meta name="keywords" content="demo, flexi auth, user authentication, codeigniter"/>
	<?php $this->load->view('includes/head'); ?> 
</head>

<body id="public_dashboard">

<div id="body_wrap">
	<!-- Header -->  
	<?php $this->load->view('includes/header'); ?> 

	<!-- Demo Navigation -->
	<?php $this->load->view('includes/demo_header'); ?> 
	
	<!-- Intro Content -->

	
	<!-- Main Content -->
	<div class="content_wrap main_content_bg">
            <div id="sub_nav_wrap" class="content">
                <div class="div-main-menu-panel">
                      <div class="div-main-panel-row">
                      	<div class="div-main-panel-33"></div>
                      	<div class="div-main-panel-33">PLAY</div>
                      	<div class="div-main-panel-33"></div>
                      </div>
                      <div class="div-main-panel-row">
                      	<div class="div-main-panel-33">MARKET</div>
                      	<div class="div-main-panel-33">HOME</div>
                      	<div class="div-main-panel-33">HELP</div>
                      </div>
                      <div class="div-main-panel-row">
                      	<div class="div-main-panel-33">CC</div>
                      	<div class="div-main-panel-33"></div>
                      	<div class="div-main-panel-33">RL</div>
                	   </div>      
                </div>
                
                <div class="div-main-submenu">
                    <div class="div-noborder-100">
                        <span class="title">News</span>
                    </div>
                    <div class="div-noborder-100">
                        <br>Game Mechanics started.</br>
                        <br>Collection data in process.</br>
                        <br>Connection now works partially.</br>
                    </div>
                </div>
            </div>
	</div>	
	
	<!-- Footer -->  
	<?php $this->load->view('includes/footer'); ?> 
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 

</body>
</html>