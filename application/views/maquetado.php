<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online</title>
        <link type="text/css" href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    	<link type="text/css" href="<?php echo base_url();?>assets/css/animsition.css" rel="stylesheet">
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<script src="<?php echo base_url();?>assets/js/external/jquery/jquery.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
	    <script src="<?php echo base_url();?>assets/js/jquery.animsition.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui.css">
        <link rel="shortcut icon" href="<?php echo base_url();?>favicon.ico" type="image/x-icon"/>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.sidr.dark.css">
        <script src="<?php echo base_url();?>assets/js/jquery.sidr.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/game.js"></script>
</head>


<body id="gamebody" class="animsition">
<div class="global">
    <!-- GLOBAL GAME BOARD, NEVER WRITE SOMETHING OFF-->
    
    <div id="selection" class="selection" style="display: none;">
        <div class="div-noborder-100" >Selection</div>
        <div>
            <a id="ok" href="#">OK</a>
        </div>
    </div>
    
    <div id="console" class="console">
        >Console
        <div id="0" class="console-line">
            Event Log Enabled
        </div>
    </div>
    
    
    <div class="scenario-top"> <!-- TOP SCENARIO UPPER BOARD SIDE (ENEMY)-->
        <!-- status box -->
        <div class="area-character">
            <div id="area-status-opp">
                <div class="area-status-name-rank">
                    <div style="display: inline-block;"><img id="opp-rank" src="#" class="area-status-rank"></img></div>
                    <div style="display: inline-block;"><span id="area-status-opp-nick">[Name]</span></div>
                </div>
                <div class="area-status-health-time">
                    <div class="area-health-img">
                        <img class="health-img" src="<?php echo base_url();?>/assets/img/hp_full.png"/>
                    </div>
                    <div class="area-health">
                        <span id="area-status-opp-health">50</span>
                    </div>
                    
                    <div class="area-timer">
                        <span id="area-status-opp-timer">50</span>
                    </div>
                </div>
                
              <div class="area-status-energy">
                    <div class="area-energy-row">
                        <div class="area-energy-left">
                            <span id="opp-en-green-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_fo.png"/></span>
                            <span id="opp-en-green-used">0</span>
                            <span id="opp-en-green-div">/</span>
                            <span id="opp-en-green-max">0</span>
                        </div>
                        <div class="area-energy-right">
                            <span id="opp-en-black-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_da.png"/></span>
                            <span id="opp-en-black-used">0</span>
                            <span id="opp-en-black-div">/</span>
                            <span id="opp-en-black-max">0</span>
                            
                        </div>
                    </div>
                    <div class="area-energy-row">
                        <div class="area-energy-33">
                            <span id="opp-en-black-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_ra.png"/></span>
                            <span id="opp-en-blue-used">0</span>
                            <span id="opp-en-blue-div">/</span>
                            <span id="opp-en-blue-max">0</span></div>
                        <div class="area-energy-33">
                            <span id="opp-en-white-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_ne.png"/>
                            </span>
                            <span id="opp-en-white-used">0</span>
                            <span id="opp-en-white-div">/</span>
                            <span id="opp-en-white-max">0</span></div>
                        <div class="area-energy-33">
                            <span id="opp-en-red-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_fa.png"/>
                            </span>
                            <span id="opp-en-red-used">0</span>
                            <span id="opp-en-red-div">/</span>
                            <span id="opp-en-red-max">0</span></div>
                    </div>    
                    <div class="area-energy-row">
                        <div class="area-energy-left">
                             <span id="opp-en-purple-img">
                                 <img src="<?php echo base_url();?>/assets/img/buttons/en/ele_ph.png"/>
                             </span>
                            <span id="opp-en-purple-used">0</span>
                            <span id="opp-en-purple-div">/</span>
                            <span id="opp-en-purple-max">0</span>
                        </div>
                        <div class="area-energy-right">
                            <span id="opp-en-yellow-img">
                                <img src="<?php echo base_url();?>/assets/img/buttons/en/ele_br.png"/>
                            </span>
                            <span id="opp-en-yellow-used">0</span>
                            <span id="opp-en-yellow-div">/</span>
                            <span id="opp-en-yellow-max">0</span>
                        </div>
                    </div>    
                       
                        
                </div>
                
                <div class="area-status-cards">
                    <div class="area-status-deck">
                        DECK
                    </div>
                    <div class="area-status-hand">
                        HAND
                    </div>
                </div>
                    
                   
            </div>
                
        </div>
             
        <!-- cards map-->
        <div class="cards-map">
          <!-- row top-->
           <div class="row-top">
                <div id="top-removal" class="zone-removal">
                   
                </div>
                <div id="top-treasure" class="zone-treasure">
                </div>
                <div id="top-homeland" class="zone-homeland">
                    
                </div>
            </div>
          <!-- row middle-->
           <div class="row-mid">
                 <div id="top-discard" class="zone-discard">
                 </div>
                <div id="top-deck"  class="zone-deck">
                        <img id="top-deck-count" title="30 cards remaining"  class="deck-dialog" src="<?php echo base_url()."/assets/img/cards/back.png";?>" class="deck"/>
                </div>
                <div id="top-first" class="zone-secondline">
                     
                </div>        
            </div>
          <!--row bottom-->  
           <div class="row-bottom">
                <div id="top-blank" class="zone-blank">
                </div>
                
                <div id="top-ambience" class="zone-ambience"> 
                   
                </div>
                
                <div id="top-second" class="zone-firstline">
                      
                </div>
            </div>
            
           
            
        </div>
    </div> <!-- END  TOP BOARD SIDE -->
    
    <div id="turn-message" class="turn-message" style="display: none;">
        <img src="<?php echo base_url();?>assets/img/buttons/yt.png"/>
    </div>
    
    <div class="scenario-bottom"> <!-- BOTTOM BOARD SIDE (PLAYER BOARD) -->
        
        
        <!-- status box -->
        <div class="area-character">
            <div id="area-my-status">
                <div class="area-status-name-rank">
                    <div style="display: inline-block;"><img id="my-rank" src="#" class="area-status-rank"></img></div>
                    <div style="display: inline-block;"><span id="area-status-my-nick">[Name]</span></div>
                </div>
                <div class="area-status-health-time">
                    <div class="area-health-img">
                        <img class="health-img" src="<?php echo base_url();?>/assets/img/hp_full.png"/>
                    </div>
                    <div class="area-health">
                        <div id="area-status-my-health">50</div>
                    </div>
                    
                    <div class="area-timer">
                        <div id="area-status-my-timer">50</div>
                    </div>
                </div>
                
                <div class="area-status-energy">
                    <div class="area-energy-row">
                        <div class="area-energy-left">
                            <span id="my-en-green-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_fo.png"/></span>
                            <span id="my-en-green-used">0</span>
                            <span id="my-en-green-div">/</span>
                            <span id="my-en-green-max">0</span>
                        </div>
                        <div class="area-energy-right">
                            <span id="my-en-black-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_da.png"/></span>
                            <span id="my-en-black-used">0</span>
                            <span id="my-en-black-div">/</span>
                            <span id="my-en-black-max">0</span>
                            
                        </div>
                    </div>
                    <div class="area-energy-row">
                        <div class="area-energy-33">
                            <span id="my-en-black-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_ra.png"/></span>
                            <span id="my-en-blue-used">0</span>
                            <span id="my-en-blue-div">/</span>
                            <span id="my-en-blue-max">0</span></div>
                        <div class="area-energy-33">
                            <span id="my-en-white-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_ne.png"/>
                            </span>
                            <span id="my-en-white-used">0</span>
                            <span id="my-en-white-div">/</span>
                            <span id="my-en-white-max">0</span></div>
                        <div class="area-energy-33">
                            <span id="my-en-red-img"><img src="<?php echo base_url();?>/assets/img/buttons/en/ele_fa.png"/>
                            </span>
                            <span id="my-en-red-used">0</span>
                            <span id="my-en-red-div">/</span>
                            <span id="my-en-red-max">0</span></div>
                    </div>    
                    <div class="area-energy-row">
                        <div class="area-energy-left">
                             <span id="my-en-purple-img">
                                 <img src="<?php echo base_url();?>/assets/img/buttons/en/ele_ph.png"/>
                             </span>
                            <span id="my-en-purple-used">0</span>
                            <span id="my-en-purple-div">/</span>
                            <span id="my-en-purple-max">0</span>
                        </div>
                        <div class="area-energy-right">
                            <span id="my-en-yellow-img">
                                <img src="<?php echo base_url();?>/assets/img/buttons/en/ele_br.png"/>
                            </span>
                            <span id="my-en-yellow-used">0</span>
                            <span id="my-en-yellow-div">/</span>
                            <span id="my-en-yellow-max">0</span>
                        </div>
                    </div>    
                       
                        
                </div>
                
                <div class="area-status-cards">
                    <div id="end-button-div" class="area-status-end-button">
                        <a id="end-turn" href="#">
                            <img id="end-button-img" src="<?php echo base_url();?>assets/img/buttons/wt.png" class="end-button-img"/>
                        </a>
                    </div>
                 </div>
                    
                   
            </div>
                
        </div>
        
        
        <!-- cards map-->
        <div class="cards-map">
           <!-- top -->
            <div class="row-top">
              <div id="bot-blank" class="zone-blank">
                    
                </div>
                
                <div id="bot-ambience" class="zone-ambience">
                    
                </div>
                
                <div id="bot-first" class="zone-firstline">
                    <div id="p2h3xertex" class="card-small">
                      <img src="<?php echo base_url()."/assets/img/cards/7.png";?>" class="card-small"/>
                     </div>
                </div>
            </div>
             <!--mid -->
            <div class="row-mid">
                 <div id="bot-discard" class="zone-discard">
                 </div>
                 <!-- -->
                <div id="bot-deck" class="zone-deck">
                    <img id="bot-deck-count" title="25 cards remaining"  class="deck-dialog" src="<?php echo base_url()."/assets/img/cards/back.png";?>" class="deck"/>
                </div>
                <!-- -->
                <div id="bot-second" class="zone-secondline">   
                </div>        
            </div>
             <!--bottom -->
              <div class="row-bottom">
                
                  
                  
                <div id="bot-removal" class="zone-removal">
                </div>
              
                <div id="bot-treasure" class="zone-treasure">
                </div>
                  
                <div id="bot-homeland" class="zone-homeland">
                   
                </div>
             </div>
            
            
        </div>
      
</div>
<div id="my-hand" class="my-hand">
        
        
</div>

    <div class="off-menu">
        <a id="simple-menu" href="#sidr">
            <img src="<?php echo base_url()."/assets/img/buttons/eng1.png"?>" />
        </a>
</div>
            


</div> <!-- END PLAYER BOARD -->








    <div id="sidr">
      <!-- Your content -->
      <ul>
         <li>
    	<a href="board/surrender">Rendirse</a>
        </li>
        <li></li>
        <li></li>
        <li></li>
        <li>
    	<a href="#">Opciones</a>
        </li>
        <li>
    	<a href="<?php echo base_url();?>auth/logout">Desconectarse</a>
        </li>
        <li>
    	<a href="<?php echo base_url();?>admin">Administración [*]</a>
        </li>
        <li>
    	<a href="<?php echo base_url();?>board/clearConsole">Clear Console [*]</a>
        </li>
        
        
      </ul>
    </div>
</body> 
