<script>
//open right menu
$(document).ready(function() {
    
  $("#gamebody").removeClass("animsition");
    
  $('#simple-menu').sidr({
      side: 'right'
    });
    
//close right menu

    $('#close-menu').click(function(){
            $.sidr();
            
       });
       

//cards remanining
$("#top-deck-count").tooltip();
$("#bot-deck-count").tooltip();

});
//lock right click
/*
$('body').on('contextmenu', function(e){ 
     return false; 
});*/
    
function boardRefresh(){
    $.post(window.location.origin+"/board/boardRefresh",
        function(response)
      	{
            console.log(JSON.parse(response));
            if(response !== null && response !== "" && response !== "[]")
            {
                var decode = JSON.parse(response);
                
                
                
                for (var key in decode['my_bo']) {
                    if (decode['my_bo'].hasOwnProperty(key)) {
                     
                     
                        switch(decode['my_bo'][key]['location'])
                        {
                            
                        case 'fl':
                            $("#bot-first").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small" style="background: url(&quote" + <?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png&quote)"/></div>');
                            break;
                        
                        case 'sl':
                            $("#bot-second").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'hl':
                        $("#bot-homeland").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'dp':
                        $("#bot-discard").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small" style="display: none"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png"/></div>');
                            break;
                        
                        case 'rem':
                        $("#bot-removal").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small" style="display: none"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png"/></div>');
                            break;
                        
                        case 'am':
                        $("#bot-ambience").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png"/></div>');
                           break;
                        
                        case 'tr':
                        $("#bot-treasure").append('<div id="'+ decode['my_bo'][key].id +'" class="card-small" style="display: none"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['my_bo'][key].num+'.png"/></div>');
                           break;
                        
                        
                        default:
                            console.log(decode['my_bo'][key]['location']);
                            break;
                        }
                }}
                
                 $("#bot-discard").children().last().toggle(true);
                 $("#bot-removal").children().last().toggle(true);
                 $("#bot-treasure").children().last().toggle(true);
                
                for (var key in decode['opp_bo']) {
                    if (decode['opp_bo'].hasOwnProperty(key)) {
                     
                     
                        switch(decode['opp_bo'][key]['location'])
                        {
                            
                        case 'fl':
                            $("#top-first").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'sl':
                            $("#top-second").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'hl':
                        $("#top-homeland").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'dp':
                        $("#top-discard").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small" style="display: none"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'rem':
                        $("#top-removal").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small" style="display: none"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                            break;
                        
                        case 'am':
                        $("#top-ambience").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                           break;
                        
                        case 'tr':
                        $("#top-treasure").append('<div id="'+ decode['opp_bo'][key].id +'" class="card-small" style="display: none"><img class="card-small" src="<?php echo base_url();?>assets/img/cards/'+decode['opp_bo'][key].num+'.png" /></div>');
                           break;
                        
                        
                        default:
                            console.log(decode['opp_bo'][key]['location']);
                            break;
                        }
                        //($.append('<div id="'+ decode[key].id +'" class="card-hand"></div>');

                    //$("#"+decode[key].id).css("background-image","url('<?php echo base_url();?>assets/img/cards/"+decode[key].num+".png')");
                }}
                
                $("#top-discard").children().last().toggle(true);
                 $("#top-removal").children().last().toggle(true);
                 $("#top-treasure").children().last().toggle(true);



              
      			
            }
        });
   
        //mark first load
        if(status < 4)
        status++;
}

function init(){
    $.post(window.location.origin+"/board/createInfoFeed",
        function(response)
      	{
            console.log(JSON.parse(response));
            if(response !== null && response !== "" && response !== "[]")
            {
                var decode = JSON.parse(response);
                $("#area-status-opp-nick").html("");
                $("#area-status-opp-nick").html(decode["opp"]);
                $("#area-status-opp-health").html("");
                $("#area-status-opp-health").html(decode["opp_hp"]);
                $("#my-rank").attr("src","<?php echo base_url();?>/assets/img/rank/"+decode["self_rank"]["rank"]+".png");
                
                $("#area-status-my-nick").html("");
                $("#area-status-my-nick").html(decode["self"]);
                $("#area-status-my-health").html("");
                $("#area-status-my-health").html(decode["self_hp"]);
                $("#opp-rank").attr("src","<?php echo base_url();?>/assets/img/rank/"+decode["opp_rank"]["rank"]+".png");
              
            }
        });
        
        if(status < 4)
        status++;
    }
    
function handRefresh(){
    $.post(window.location.origin+"/board/createHandFeed",
        function(response)
      	{
            
            if(response !== null && response !== "" && response !== "[]")
            {
                var decode = JSON.parse(response);
                var hand = $('#my-hand');
                hand.html("");
                for (var key in decode) {
                    if (decode.hasOwnProperty(key)) {
                     
                     hand.append('<div id="'+ decode[key].id +'" class="card-hand"></div>');
                     $("#"+decode[key].id).css("background-image","url('<?php echo base_url();?>assets/img/cards/"+decode[key].num+".png')");
                }}
      							 
      							   
             }
         }
    );
    
    if(status < 4)
    status++;
 }

function energyRefresh(){

$.post(window.location.origin+"/board/createEnergyFeed",
        function(response)
      	{
            console.log(JSON.parse(response));
            if(response !== null && response !== "" && response !== "[]")
            {
                var decode = JSON.parse(response);
                
                if(decode["my_max"]["ne"] > 0){
                    $("#my-en-white-img").fadeIn();
                    $("#my-en-white-used").fadeIn();
                    $("#my-en-white-used").html("");
                    $("#my-en-white-used").html(decode["my_used"]["ne_used"]);
                    $("#my-en-white-div").fadeIn();
                    $("#my-en-white-max").fadeIn();
                    $("#my-en-white-max").html("");
                    $("#my-en-white-max").html(decode["my_max"]["ne"]);
                }
                
                if(decode["my_max"]["fa"] > 0){
                    $("#my-en-red-img").fadeIn();
                    $("#my-en-red-used").fadeIn();
                    $("#my-en-red-used").html("");
                    $("#my-en-red-used").html(decode["my_used"]["fa_used"]);
                    $("#my-en-red-div").fadeIn();
                    $("#my-en-red-max").fadeIn();
                    $("#my-en-red-max").html("");
                    $("#my-en-red-max").html(decode["my_max"]["fa"]);
                }
                
                if(decode["my_max"]["br"] > 0){
                    $("#my-en-yellow-img").fadeIn();
                    $("#my-en-yellow-used").fadeIn();
                    $("#my-en-yellow-used").html("");
                    $("#my-en-yellow-used").html(decode["my_used"]["br_used"]);
                    $("#my-en-yellow-div").fadeIn();
                    $("#my-en-yellow-max").fadeIn();
                    $("#my-en-yellow-max").html("");
                    $("#my-en-yellow-max").html(decode["my_max"]["br"]);
                }
                
                if(decode["my_max"]["ra"] > 0){
                    $("#my-en-blue-img").fadeIn();
                    $("#my-en-blue-used").fadeIn();
                    $("#my-en-blue-used").html("");
                    $("#my-en-blue-used").html(decode["my_used"]["ra_used"]);
                    $("#my-en-blue-div").fadeIn();
                    $("#my-en-blue-max").fadeIn();
                    $("#my-en-blue-max").html("");
                    $("#my-en-blue-max").html(decode["my_max"]["ra"]);
                }
                
                if(decode["my_max"]["fo"] > 0){
                    $("#my-en-green-img").fadeIn();
                    $("#my-en-green-used").fadeIn();
                    $("#my-en-green-used").html("");
                    $("#my-en-green-used").html(decode["my_used"]["fo_used"]);
                    $("#my-en-green-div").fadeIn();
                    $("#my-en-green-max").fadeIn();
                    $("#my-en-green-max").html("");
                    $("#my-en-green-max").html(decode["my_max"]["fo"]);
                }
                
                if(decode["my_max"]["ph"] > 0){
                    $("#my-en-purple-img").fadeIn();
                    $("#my-en-purple-used").fadeIn();
                    $("#my-en-purple-used").html("");
                    $("#my-en-purple-used").html(decode["my_used"]["ph_used"]);
                    $("#my-en-purple-div").fadeIn();
                    $("#my-en-purple-max").fadeIn();
                    $("#my-en-purple-max").html("");
                    $("#my-en-purple-max").html(decode["my_max"]["ph"]);
                }
                
                if(decode["my_max"]["da"] > 0){
                    $("#my-en-black-img").fadeIn();
                    $("#my-en-black-used").fadeIn();
                    $("#my-en-black-used").html("");
                    $("#my-en-black-used").html(decode["my_used"]["da_used"]);
                    $("#my-en-black-div").fadeIn();
                    $("#my-en-black-max").fadeIn();
                    $("#my-en-black-max").html("");
                    $("#my-en-black-max").html(decode["my_max"]["da"]);
                }
                //--------------------------------------------
                
                if(decode["opp_max"]["ne"] > 0){
                    $("#opp-en-white-img").fadeIn();
                    $("#opp-en-white-used").fadeIn();
                    $("#opp-en-white-used").html("");
                    $("#opp-en-white-used").html(decode["opp_used"]["ne_used"]);
                    $("#opp-en-white-div").fadeIn();
                    $("#opp-en-white-max").fadeIn();
                    $("#opp-en-white-max").html("");
                    $("#opp-en-white-max").html(decode["opp_max"]["ne"]);
                }
                
                if(decode["opp_max"]["fa"] > 0){
                    $("#opp-en-red-img").fadeIn();
                    $("#opp-en-red-used").fadeIn();
                    $("#opp-en-red-used").html("");
                    $("#opp-en-red-used").html(decode["opp_used"]["fa_used"]);
                    $("#opp-en-red-div").fadeIn();
                    $("#opp-en-red-max").fadeIn();
                    $("#opp-en-red-max").html("");
                    $("#opp-en-red-max").html(decode["opp_max"]["fa"]);
                }
                
                if(decode["opp_max"]["br"] > 0){
                    $("#opp-en-yellow-img").fadeIn();
                    $("#opp-en-yellow-used").fadeIn();
                    $("#opp-en-yellow-used").html("");
                    $("#opp-en-yellow-used").html(decode["opp_used"]["br_used"]);
                    $("#opp-en-yellow-div").fadeIn();
                    $("#opp-en-yellow-max").fadeIn();
                    $("#opp-en-yellow-max").html("");
                    $("#opp-en-yellow-max").html(decode["opp_max"]["br"]);
                }
                
                if(decode["opp_max"]["ra"] > 0){
                    $("#opp-en-blue-img").fadeIn();
                    $("#opp-en-blue-used").fadeIn();
                    $("#opp-en-blue-used").html("");
                    $("#opp-en-blue-used").html(decode["opp_used"]["ra_used"]);
                    $("#opp-en-blue-div").fadeIn();
                    $("#opp-en-blue-max").fadeIn();
                    $("#opp-en-blue-max").html("");
                    $("#opp-en-blue-max").html(decode["opp_max"]["ra"]);
                }
                
                if(decode["opp_max"]["fo"] > 0){
                    $("#opp-en-green-img").fadeIn();
                    $("#opp-en-green-used").fadeIn();
                    $("#opp-en-green-used").html("");
                    $("#opp-en-green-used").html(decode["opp_used"]["fo_used"]);
                    $("#opp-en-green-div").fadeIn();
                    $("#opp-en-green-max").fadeIn();
                    $("#opp-en-green-max").html("");
                    $("#opp-en-green-max").html(decode["opp_max"]["fo"]);
                }
                
                if(decode["opp_max"]["ph"] > 0){
                    $("#opp-en-purple-img").fadeIn();
                    $("#opp-en-purple-used").fadeIn();
                    $("#opp-en-purple-used").html("");
                    $("#opp-en-purple-used").html(decode["opp_used"]["ph_used"]);
                    $("#opp-en-purple-div").fadeIn();
                    $("#opp-en-purple-max").fadeIn();
                    $("#opp-en-purple-max").html("");
                    $("#opp-en-purple-max").html(decode["opp_max"]["ph"]);
                }
                
                if(decode["opp_max"]["da"] > 0){
                    $("#opp-en-black-img").fadeIn();
                    $("#opp-en-black-used").fadeIn();
                    $("#opp-en-black-used").html("");
                    $("#opp-en-black-used").html(decode["opp_used"]["da_used"]);
                    $("#opp-en-black-div").fadeIn();
                    $("#opp-en-black-max").fadeIn();
                    $("#opp-en-black-max").html("");
                    $("#opp-en-black-max").html(decode["opp_max"]["da"]);
                }
                    
                    
            }
    });
    
    if(status < 4)
    status++;
}
function stateRefresh(){
    
}

$(document).ready(function()
{
   var msg = 0;
   var status = 0; 
   var delay = 0;
   var movement = 0;
   var lock = 0;
   var opp_actions = 'locked';
   var opp_refresh = 5000;
   var obj = '';
    //initial data remap
   init();
   boardRefresh();
   handRefresh();
   energyRefresh();
   
    //allow play/turn end
    
    refreshing = setInterval(function() {
        console.log("refreshing status...");
        

        $.post(window.location.origin+"/board/checkGameState",
        function(response)
      	{
            //handRefresh();
            energyRefresh();
            //console.log(JSON.parse(response));
            if(response === 'choose_hand'){
                
                console.log("should be choosing hand");
                if(status !== 'exchange-all-stand' && status >= 4) status = 'exchange-all';
                exchangeHand();
                
            }
            
            //standing refers as preventing your turn to advance before enemy actions show up
            else if(response === 'my_turn' && opp_actions !== 'standing'){
                opp_actions = 'locked';
                if(msg === 0)
                {
                    $("#turn-message").fadeIn(300).delay(1000).fadeOut(500);
                    msg = 1;
                }
                
            
                
                $("#end-button-img").attr("src","<?php echo base_url();?>assets/img/buttons/et.png");
                console.log("on my turn");
                $(".scenario-top").toggleClass("scenario-active",false);
                $(".scenario-bottom").toggleClass("scenario-active",true);


                
                $(".card-hand").draggable({
                    revert: function(event, ui) {
                            
                            $(this).data("uiDraggable").originalPosition = {
                                top : 0,
                                left : 0
                            };
                            // return boolean
                            return !event;
                            }

                });

                $(".card-hand").on( "dragstop", function( event, ui ) {
                    if(delay === 0 && ui.position.top !== 0)
                    {
                        console.log("clicked card");
                        
                        if(ui.position.top <= -100){
                            console.log("playing detected");
                            delay = 1;
                            setTimeout(playdelay,1500);
                            id_card = $(this).attr("id");
                            allocate = $(this);
                            console.log("preparing to send " + id_card);
                            
                             $.post(window.location.origin+"/board/playCard",
                             {
                                 id : id_card
                             },
                                function(response)
                                {
                                    console.log(response);
                                    if(response !== null){
                                        var play = JSON.parse(response);
                                        if(play["result"] === 'play'){
                                           
                                            obj = '<div id="'+ play["id_board"] +'" class="card-small" style="background: url(&quot;<?php echo base_url();?>/assets/img/cards/'+play["card_num"]+'.png&quot;)  0 0 / 100% auto"><div class="attack"></div><div class="health"></div></div>';

                                            console.log('play accepted, type: '+play["card_type"]);
                                            
                                            

                                                switch(play["card_type"]){

                                                case '0':   $("#bot-first").append(obj);
                                                            console.log("moving "+$(this));
                                                            break;

                                                case '1':   $("#bot-treasure").append(obj);
                                                            break;

                                                case '2':   $("#bot-homeland").append(obj);
                                                            break;

                                                case '3':   $("#bot-discard").append(obj);
                                                            break;

                                                case '4':    $("#bot-first").append(obj);
                                                            console.log('weapon: requires id analysis, unavailable');
                                                            break;

                                                case '5':    $("#bot-first").append(obj);
                                                            break;



                                                default:    break;

                                                }
                                            
                                            
                                        }

                                    }
                                });
                            
                        }
                        
                        else if(ui.position.top > -100){
                            console.log("playing aborted");
                            delay = 1;
                            setTimeout(playdelay,300);
                           
                        }
                       
                    }
                     else{
                            //lock
                           //console.log("cant play due to delay");
                           
                     }
            
                    
                    
                });
                
            }
            
            else if(response === 'opp_turn'){
                msg = 0;
                $("#end-button-img").attr("src","<?php echo base_url();?>assets/img/buttons/wt.png");
                console.log("on oponent turn");
                $(".scenario-top").toggleClass("scenario-active",true);
                $(".scenario-bottom").toggleClass("scenario-active",false);
                
                    

                        if(opp_actions !== 'locked' || opp_actions !== 'standing'){
                          $.post(window.location.origin+"/board/movementRefresh",
                          {
                              mov : movement
                          },
                          function(response)
                          {
                              console.log("retrieving_movements");
                              
                          });
                      }
              
                    
                

            }


            


        });
        function playdelay(){
            console.log("delay finished");
            delay = 0;
            }
        
        
    }
    , 5000);
    //console refresh state
    timer = setInterval(function() {
        var last = parseInt($('#console .console-line:last-child').attr("id"));

        $.post(window.location.origin+"/board/refreshConsole",
        {
             id: last
        },
            function(response)
            {
                if(response !== null && response !== "" && response !== "Disconnected")
                {
                    var decode = JSON.parse(response);

                                                    var console = $('#console');

                                                            for (var key in decode) {
                                                            if (decode.hasOwnProperty(key)) {

                                                                console.append('<div id="'+ decode[key].id_msg +'" class="console-line">' + decode[key].msg + '</div>');

                                                             }
                                                      }
                }

                }).fail(function(XMLHttpRequest, textStatus, errorThrown){ 
                    alert('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
                  });
                      //end console progress
    
         while($("#console").children().size() > 25){
                $("#console").children().first().remove();
         }             
    
    //on pla 
    }, 5000);
    
    
    function exchangeHand(){
        if(status === 'exchange-all'){
            console.log("algo?");
            $("#selection").html("");
            $("#my-hand").find(".card-hand").clone().appendTo("#selection");
            
            $("#selection").append('<div><a id="ok" href="#">OK</a></div>');
         
            
            
            $("#selection").fadeIn();
            status = 'exchange-all-stand';

        }
        
      $("#ok").click(function()
      {
        status = 5;
        $.post(window.location.origin+"/board/toggleTimer",function(response)
      	{ });
        
        $("#selection").fadeOut();
      });
    }
    
        
   $("#ok").click(function()
   {
       $("#selection").fadeOut();
   });
   
   
   $("#end-turn").click(function()
    {
       $.post(window.location.origin+"/board/endTurn",
        function(response)
      	{
            
            if(response === 'turn_ended'){
                console.log("ending turn...");
                $("#end-button-img").attr("src","<?php echo base_url();?>assets/img/buttons/et.png");
                $(".scenario-top").toggleClass("scenario-active",true);
                $(".scenario-bottom").toggleClass("scenario-active",false);
                
            }
            
            else if(response === 'opponent_turn')
            {

                console.log("cant end turn...");
            }
        });
     
    });

  
  $(".animsition").animsition({
  
    inClass               :   'fade-in',
    outClass              :   'fade-out',
    inDuration            :    1500,
    outDuration           :    800,
    linkElement           :   '.animsition-link',
    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
    loading               :    true,
    loadingParentElement  :   'body', //animsition wrapper element
    loadingClass          :   'animsition-loading',
    unSupportCss          : [ 'animation-duration',
                              '-webkit-animation-duration',
                              '-o-animation-duration'
                            ],
    //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    
    overlay               :   false,
    
    overlayClass          :   'animsition-overlay-slide',
    overlayParentElement  :   'body'
  });
  
  //$(".animsition").removeClass("animsition");
});



</script>