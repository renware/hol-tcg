<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Register</title>
	<meta name="description" content="Hanakomi Cards Online, TCG, HOL"/> 
	<meta name="keywords" content="hanakomi, tcg, hol, game, cards"/>
	<?php $this->load->view('includes/head'); ?> 
</head>

<body class="global-main" id="register">

<div id="body_wrap">
	<!-- Header -->  
	<?php $this->load->view('includes/header'); ?> 

	<!-- Demo Navigation -->
	<?php $this->load->view('includes/demo_header'); ?> 
	
	<!-- Intro Content -->
	<div class="content_wrap intro_bg">
		<div class="content clearfix">
		</div>
	</div>
	
	<!-- Main Content -->
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url()); ?>  	
					
					<div class="div-small-bordered-100">
                                            <div class="div-noborder-100">
                                                <div class="title">
                                                New User Registration
                                            </div>
                                            </div>
                                            
						<hr/>
                                                <ul>
							<li class="info_req">
								<label for="email_address">Email Address:</label>
								<input class="reg-input-text" type="text" id="email_address" name="register_email_address" value="<?php echo set_value('register_email_address');?>" class="tooltip_trigger"
									title="This demo requires that upon registration, you will need to activate your account via clicking a link that is sent to your email address."
								/>
							</li>
							<li class="info_req">
								<label for="username">Username:</label>
								<input class="reg-input-text" type="text" id="username" name="register_username" value="<?php echo set_value('register_username');?>" class="tooltip_trigger"
									title="Set a username that can be used to login with."
								/>
							</li>
							
							<li class="info_req">
								<label for="password">Password:</label>
								<input class="reg-input-text" type="password" id="password" name="register_password" value="<?php echo set_value('register_password');?>"/>
							</li>
							<li class="info_req">
								<label for="confirm_password">Confirm Password:</label>
								<input class="reg-input-text" type="password" id="confirm_password" name="register_confirm_password" value="<?php echo set_value('register_confirm_password');?>"/>
							</li>
                                                        <li class="info_req">
								<label>By registering you agree with our Terms Of Service.</label>
								
							</li>
                                                        <li class="info_req">
								<hr/>
								<input type="submit" name="register_user" id="submit" value="Submit" class="reg-input-ok"/>
                                                                <input type="button" name="exit" id="exit" value="Back" class="reg-input-no"/>
							</li>
						</ul>
                                        </div>
					
					
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
	
	<!-- Footer -->  
	<?php $this->load->view('includes/footer'); ?> 
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 

</body>
</html>

<script>
    $("#exit").click(function(){
        window.location.href = '<?php echo base_url();?>home';
    });
    </script>