<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Free Trading Card Game</title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/> 
	<meta name="keywords" content="flexi auth, user authentication, codeigniter"/>
	<?php $this->load->view('includes/head'); ?> 
</head>

<body id="admin">

<div id="body_wrap">
	<!-- Header -->  
	<?php $this->load->view('includes/header'); ?> 

	<!-- Demo Navigation -->
	<?php $this->load->view('includes/demo_header'); ?> 
	

	<!-- Intro Content -->

        <div class="content_wrap intro_bg">
		<div class="content clearfix">
			<div class="div-noborder-100">
				Admin Panel
				<p>Lugar solo accesible a administradores, permite gestionar la mayoría de los aspectos que tengan que ver con la base de datos del juego.</p>
                                <p>Este lugar comenzará a funcionar tempranamente para facilitar la construcción del juego.</p>
                        </div>		
		</div>
	</div>
	
	<!-- Main Content -->
	<div class="content_wrap main_content_bg">
            <?php
            if(empty($output))
            {
               
            ?>
            <div id="sub_nav_wrap" class="content">
                <a href="admin/cards">Gestionar Biblioteca de cartas</a>
            </div>
            
            <div id="sub_nav_wrap" class="content"">
                <a href="admin/users">Gestionar Usuarios</a>
            </div>
            
            <div id="sub_nav_wrap" class="content"">
                <a href="admin/collections">Gestionar Colecciones de Usuarios</a>
            </div>
            
            <?php
            }
            if(!empty($output))
            {
            ?>

            <div id="sub_nav_wrap" class="content">
            <?php        echo $output; ?>
                   
                <div id="sub_nav_wrap" class="content">
                    <a href="../admin">Atrás</a>
                </div>   
             
            </div>
            <?php
            }
            ?>
            
           
	</div>	
        
        

</div>

<!-- Footer -->  
<?php $this->load->view('includes/footer'); ?> 

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 

</body>
</html>