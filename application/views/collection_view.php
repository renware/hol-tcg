<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>Hanakomi Cards Online - Home</title>
	<meta name="description" content="flexi auth, the user authentication library designed for developers."/> 
	<meta name="keywords" content="demo, flexi auth, user authentication, codeigniter"/>
	<script src="<?php echo base_url();?>assets/js/external/jquery/jquery.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <?php $this->load->view('includes/head'); ?> 
        
        <link type="text/css" href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui.css">
       
        
</head>

<body id="public_dashboard">

<div id="body_wrap">
	<!-- Header -->  
	<?php $this->load->view('includes/header'); ?> 

	<!-- Demo Navigation -->
	<?php $this->load->view('includes/demo_header'); ?> 
	
	<!-- Intro Content -->
        <div class="content_wrap intro_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>My Collection</h2>
			</div>		
		</div>
	</div>
	
	
	<!-- Main Content -->
        
        <div class="content_wrap main_content_bg">
            <div id="#collection-area" class="collection-area">
              <div class="collection-line">
                <?php
                $index = 0;
                foreach($collection as $card)
                {
                    if($index > 4){
                        echo '</div><div class="collection-line">';
                        $index = 0;

                    }
                $index++;
                ?>
                <div class="content grid">
                   <li>
                     <img class="card-small" src="<?php echo base_url();?>assets/img/cards/<?php echo $card["id_card"];?>.png"/>
                   </li>
                      Cantidad : <?php  echo $card["amount"]?>

                </div>

                <?php
                }
                ?>
             </div>
            </div>
            <div id="deck" class="deck-builder-area">
                 <?php
                $index = 0;
                foreach($deck as $card)
                {
                    
                $index++;
                ?> 
                <div class="deck-card-line">
                    <?php echo $card["title"];?>
                    <div class="deck-card-line-copies">
                        <?php echo $card["count"];?>
                    </div>
                </div>
                <?php
                }
                ?>
                
            </div>
        </div>
        
	
	<!-- Footer -->  
	<?php $this->load->view('includes/footer'); ?> 
</div>

<!-- Scripts -->  
<?php $this->load->view('includes/scripts'); ?> 

</body>
</html>






  <script>
  $(function() {
    $( "#collection-area" ).draggable({ revert: true });
  });
  </script>
  
  

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    $( ".card-small" ).click(function()
    {
      $($this).css("border","1px solid green");
    });
    
    $( ".card-small" ).draggable({ revert: true, helper: "clone" });
  });
  </script>
  
  
  <script>
  $(function() {
    // there's the gallery and the trash
    var $gallery = $( "#collection-area" ),
      $trash = $( "#deck" );
 
    // let the gallery items be draggable
    $( "li", $gallery ).draggable({
      cancel: "a.ui-icon", // clicking an icon won't initiate dragging
      revert: "invalid", // when not dropped, the item will revert back to its initial position
      containment: "document",
      helper: "clone",
      cursor: "move"
    });
 
    // let the trash be droppable, accepting the gallery items
    $trash.droppable({
      accept: "#collection-area > li",
      activeClass: "ui-state-highlight",
      drop: function( event, ui ) {
        deleteImage( ui.draggable );
      }
    });
 
    // let the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
      accept: "##collection-area li",
      activeClass: "custom-state-active",
      drop: function( event, ui ) {
        recycleImage( ui.draggable );
      }
    });
 
    // image deletion function
    var recycle_icon = "<a href='link/to/recycle/script/when/we/have/js/off' title='Recycle this image' class='ui-icon ui-icon-refresh'>Recycle image</a>";
    function deleteImage( $item ) {
      $item.fadeOut(function() {
        var $list = $( "ul", $trash ).length ?
          $( "ul", $trash ) :
          $( "<ul class='gallery ui-helper-reset'/>" ).appendTo( $trash );
 
        $item.find( "a.ui-icon-trash" ).remove();
        $item.append( recycle_icon ).appendTo( $list ).fadeIn(function() {
          $item
            .animate({ width: "48px" })
            .find( "img" )
              .animate({ height: "36px" });
        });
      });
    }
 
    // image recycle function
    var trash_icon = "<a href='link/to/trash/script/when/we/have/js/off' title='Delete this image' class='ui-icon ui-icon-trash'>Delete image</a>";
    function recycleImage( $item ) {
      $item.fadeOut(function() {
        $item
          .find( "a.ui-icon-refresh" )
            .remove()
          .end()
          .css( "width", "96px")
          .append( trash_icon )
          .find( "img" )
            .css( "height", "72px" )
          .end()
          .appendTo( $gallery )
          .fadeIn();
      });
    }
 
    // image preview function, demonstrating the ui.dialog used as a modal window
    function viewLargerImage( $link ) {
      var src = $link.attr( "href" ),
        title = $link.siblings( "img" ).attr( "alt" ),
        $modal = $( "img[src$='" + src + "']" );
 
      if ( $modal.length ) {
        $modal.dialog( "open" );
      } else {
        var img = $( "<img alt='" + title + "' width='384' height='288' style='display: none; padding: 8px;' />" )
          .attr( "src", src ).appendTo( "body" );
        setTimeout(function() {
          img.dialog({
            title: title,
            width: 400,
            modal: true
          });
        }, 1 );
      }
    }
 
    // resolve the icons behavior with event delegation
    $( "ul.gallery > li" ).click(function( event ) {
      var $item = $( this ),
        $target = $( event.target );
 
      if ( $target.is( "a.ui-icon-trash" ) ) {
        deleteImage( $item );
      } else if ( $target.is( "a.ui-icon-zoomin" ) ) {
        viewLargerImage( $target );
      } else if ( $target.is( "a.ui-icon-refresh" ) ) {
        recycleImage( $item );
      }
 
      return false;
    });
  });
  </script>
