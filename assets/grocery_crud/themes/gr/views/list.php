
<table cellpadding="0" cellspacing="0" border="0" class="display groceryCrudTable " id="<?php echo $table_id=uniqid(); ?>">
        <thead>
                <tr>
                        <?php foreach($columns as $column){?>
                                <th><?php echo $column->display_as; ?></th>
                        <?php }?>
                        <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
                        <th class='action'><?php echo $this->l('list_actions'); ?></th>
                        <?php }?>
                </tr>
        </thead>
        <tbody>
                <?php foreach($list as $num_row => $row){ ?>
                <tr id='row-<?php echo $num_row?>'>
                        <?php foreach($columns as $column){?>
                                <td><?php echo $row->{$column->field_name}?></td>
                        <?php }?>
                        <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
                        <td class='actions'>
                                <?php
                                if(!empty($row->action_urls)){
                                        foreach($row->action_urls as $action_unique_id => $action_url){
                                                $action = $actions[$action_unique_id];
                                ?>
                                                <a href="<?php echo $action_url; ?>" class="btn <?php echo $action->css_class; ?> " role="button">
                                                        <i class="fa  <?php echo $action->image_url; ?> <?php echo $action_unique_id;?>"></i>&nbsp;<?php echo $action->label; ?>
                                                </a>
                                <?php }
                                }
                                ?>
                                <?php if(!$unset_read){?>
                                        <a href="<?php echo $row->read_url?>" class="btn btn-info" role="button">
                                                <i class="fa fa-sign-out"></i> &nbsp;<?php echo $this->l('list_view'); ?>
                                        </a>
                                <?php }?>

                                <?php if(!$unset_edit){?>
                                        <a href="<?php echo $row->edit_url?>" class="btn btn-warning" role="button">
                                                <i class="fa fa-pencil"></i>&nbsp;<?php echo $this->l('list_edit'); ?>
                                        </a>
                                <?php }?>
                                <?php if(!$unset_delete){?>
                                        <a onclick = "javascript: return delete_row('<?php echo $row->delete_url?>', '<?php echo $num_row?>')"
                                                href="javascript:void(0)" class="btn btn-danger" role="button">
                                                <i class="fa fa-trash-o"></i>&nbsp;<?php echo $this->l('list_delete'); ?>
                                        </a>
                                <?php }?>
                        </td>
                        <?php }?>
                </tr>
                <?php }?>
        </tbody>
        <tfoot>
                <tr>
                        <?php foreach($columns as $column){?>
                                <th><input type="text" name="<?php echo $column->field_name; ?>" placeholder="<?php echo $this->l('list_search').' '.$column->display_as; ?>" class="search_<?php echo $column->field_name; ?>" /></th>
                        <?php }?>
                        <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
                                <th>
                                        <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only floatR refresh-data" role="button" data-url="<?php echo $ajax_list_url; ?>">
                                                <span class="ui-button-icon-primary ui-icon ui-icon-refresh"></span><span class="ui-button-text">&nbsp;</span>
                                        </button>
                                        <a href="javascript:void(0)" role="button" class="clear-filtering ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary floatR">
                                                <span class="ui-button-icon-primary ui-icon ui-icon-arrowrefresh-1-e"></span>
                                                <span class="ui-button-text"><?php echo $this->l('list_clear_filtering');?></span>
                                        </a>
                                </th>
                        <?php }?>
                </tr>
        </tfoot>
</table>
<script>
    addEventListener("DOMContentLoaded",function(){
        var width = document.getElementById('<?php echo $table_id; ?>').offsetWidth;
        document.getElementById('data-table').style.width=width+"px";
        $(".fg-toolbar").attr("style","width:"+width+"px");
        $(".panel-default").attr("style","overflow: auto; max-height:"+($(window).height()- 130)+"px;");
    },false);
</script>   